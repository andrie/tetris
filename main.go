package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/gdamore/tcell/v2"
)

type block struct {
	name string
	x    int
	pos  []int
}

type game struct {
	die    bool
	x, y, pos   int
	blocks []block
	tiles  []bool
}

func initGame() *game {
	const (
		x, y int = 20, 10
	)

	var g *game

	g = &game{
		die:    false,
		x:      x,
		y:      y,
		pos:      0,
		blocks: make([]block, 7),
		tiles:  make([]bool, x*y),
	}

	initBlocks(g)
	return g
}

func initScreen() tcell.Screen {
	var (
		s   tcell.Screen
		err error
	)

	s, err = tcell.NewScreen()
	panicIf(err)

	err = s.Init()
	panicIf(err)

	return s
}

func initBlocks(g *game) {
	g.blocks[0] = newBlock("I", 1, []int{0, 1, 2, 3})
	g.blocks[1] = newBlock("O", 2, []int{0, 1, 2, 3})
	g.blocks[2] = newBlock("T", 3, []int{1, 3, 4, 5})
	g.blocks[3] = newBlock("S", 3, []int{1, 2, 3, 4})
	g.blocks[4] = newBlock("Z", 3, []int{0, 1, 4, 5})
	g.blocks[5] = newBlock("J", 2, []int{1, 3, 4, 5})
	g.blocks[6] = newBlock("L", 2, []int{0, 2, 4, 5})
	randomBlocks(g)
}

func panicIf(err error) {
	if err != nil {
		panic(fmt.Errorf("%+v", err))
	}
}

func screenPrintf(s tcell.Screen, x, y int, f string, a ...any) {
	var (
		i int
		r rune
	)

	for i, r = range []rune(fmt.Sprintf(f, a...)) {
		s.SetContent(x+i, y, r, nil, tcell.StyleDefault)
	}
}

func newBlock(name string, x int, pos []int) block {
	return block{
		name: name,
		x:    x,
		pos:  pos,
	}
}

func randomBlocks(g *game) {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(g.blocks), func(i, j int) {
		g.blocks[i], g.blocks[j] = g.blocks[j], g.blocks[i]
	})
}

func setTiles(g *game) {
	var (
		x, i, n int
	)

	for i, _ = range g.tiles {
		g.tiles[i] = false
	}

	for _, n = range g.blocks[0].pos {
		x = n / g.blocks[0].x
		g.tiles[n+x*g.x-x*g.blocks[0].x+g.pos] = true
	}
}

func draw(s tcell.Screen, g *game) {
	var (
		x, y, i int
		r       rune
	)

	setTiles(g)

	for x, y, i = 0, 0, 0; y < g.y; {
		switch true {
		case g.tiles[i]:
			r = '*'
		default:
			r = '▓'
		}

		s.SetContent(x, y, r, nil, tcell.StyleDefault)

		i++
		x++
		if x >= g.x {
			x = 0
			y++
		}
	}
}

func handle(s tcell.Screen, g *game, ev tcell.Event) bool {
	var evk *tcell.EventKey

	switch ev.(type) {
	case *tcell.EventResize:
		s.Sync()
	case *tcell.EventKey:
		evk = ev.(*tcell.EventKey)

		if evk.Rune() == 'q' || evk.Key() == tcell.KeyCtrlC {
			return false
		}

			switch evk.Rune() {
			case 'W', 'w':
				g.pos -= g.x
			case 'A', 'a':
				g.pos -= 1
			case 'S', 's':
				g.pos += g.x
			case 'D', 'd':
				g.pos += 1
			}
	}

	return true
}

func gameLoop(s tcell.Screen, g *game) {
	var (
		ev   tcell.Event
		evch chan tcell.Event
		quit chan struct{}
	)

	evch = make(chan tcell.Event)
	quit = make(chan struct{})

	go s.ChannelEvents(evch, quit)

	for {
		draw(s, g)
		s.Show()

		select {
		case ev = <-evch:
			if !handle(s, g, ev) {
				return
			}
		case <-time.After(time.Second):
		}
	}
}

func main() {
	var (
		s tcell.Screen
		g *game
	)

	s = initScreen()
	defer func() {
		s.Fini()
		fmt.Print("\033[H\033[2J")
	}()

	g = initGame()
	gameLoop(s, g)
}
